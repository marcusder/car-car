from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import Technicians, AutomobileVO, Appointment
from common.json import ModelEncoder


class TechniciansListEncoder(ModelEncoder):
    model = Technicians
    properties = [
                  "first_name",
                  "last_name",
                  "employee_id",
                  "id"]


class AutomobileVOListEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href",
                  "vin",
                  "sold"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date_time",
                  "reason",
                  "vip",
                  "status",
                  "customer",
                  "vin",
                  "technician",
                  "id"
                  ]

    encoders = {"technician": TechniciansListEncoder()}


@require_http_methods(["GET", "POST"])
def list_technician(request):
    if request.method == "GET":
        technicians = Technicians.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechniciansListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        technician = Technicians.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechniciansListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def detail_technician(request, id):
    if request.method == "GET":
        technicians = Technicians.objects.get(id=id)
        return JsonResponse(technicians,
                            encoder=TechniciansListEncoder,
                            safe=False)
    else:
        count, _ = Technicians.objects.filter(id=id).delete()
        return JsonResponse({"Deleted": count > 0})


@require_http_methods(["GET", "POST"])
def list_appointment(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technicians.objects.get(employee_id=content['technician'])
            content['technician'] = technician
        except Technicians.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid employee_id"},
                status=400,
            )

        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def detail_appointments(request, id):
    if request.method == "GET":
        appointments = Appointment.objects.get(id=id)
        return JsonResponse(appointments,
                            encoder=AppointmentListEncoder,
                            safe=False)
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})


@require_http_methods(["PUT"])
def cancelAppointment(request, id):
    if request.method == "PUT":
        content = json.loads(request.body)
        if content["status"] == "Cancelled":
            Appointment.objects.filter(id=id).update(**content)

            appointments = Appointment.objects.get(id=id)
            appointments.cancelled()
            return JsonResponse(appointments,
                                encoder=AppointmentListEncoder,
                                safe=False)


@require_http_methods(["PUT"])
def finishAppointment(request, id):
    if request.method == "PUT":
        content = json.loads(request.body)
        if content["status"] == "Finished":
            Appointment.objects.filter(id=id).update(**content)

            appointments = Appointment.objects.get(id=id)
            appointments.finished()
            return JsonResponse(appointments,
                                encoder=AppointmentListEncoder,
                                safe=False)
