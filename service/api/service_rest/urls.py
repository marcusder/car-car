from django.urls import path
from .views import list_technician, detail_technician, detail_appointments, finishAppointment, cancelAppointment, list_appointment


urlpatterns = [
    path("technicians/", list_technician, name="list_technician",),
    path("technicians/<int:id>/", detail_technician, name="detail_technician"),
    path("appointments/", list_appointment, name="list_appointment"),
    path("appointments/<int:id>/", detail_appointments, name="detail_appointments"),
    path("appointments/<int:id>/cancel/", cancelAppointment, name="cancel_appointments"),
    path("appointments/<int:id>/finish/", finishAppointment, name="finish_appointments"),

]
