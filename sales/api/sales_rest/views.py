from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder



class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "address",
        "phone_number",
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
            "id",
            "vin",
            "sold",
    ]

class SalesEncoder(ModelEncoder):
    model = Sale
    properties = [
            "id",
            "price",
            "automobile",
            "salesperson",
            "customer",
    ]
    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }


#------------------------------------- Salespeople -------------------------------------#

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
              encoder = SalespersonEncoder)
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Error creating salesperson"})
            response.status_code = 400
            return response


@require_http_methods(["DELETE"])
def api_delete_salesperson(request, id):
    try:
        salesperson = Salesperson.objects.get(id=id)
        salesperson.delete()
        return JsonResponse({"message": "Salesperson deleted successfully"})
    except Salesperson.DoesNotExist:
        return JsonResponse(
            {"message": "Salesperson does not exist"},
            status=404,
        )




#------------------------------------- Customers -------------------------------------#

@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
              encoder = CustomerEncoder)
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Error creating customer"})
            response.status_code = 400
            return response
        


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    try:
        customer = Customer.objects.get(id=id)
        customer.delete()
        return JsonResponse({"message": "customer deleted successfully"})
    except Customer.DoesNotExist:
        return JsonResponse(
            {"message": "customer does not exist"},
            status=404,
        )

    

#------------------------------------- Sales -------------------------------------#

@require_http_methods(["GET", "POST", "PUT"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
              encoder = SalesEncoder)
    else:
        content = json.loads(request.body)

        try:
            salesperson = Salesperson.objects.get(employee_id=content["salesperson"])
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=400,
            )

        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
            automobile.sold = True
            automobile.save()
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Vin does not exist"},
                status=400,
            )

        try:
            customer = Customer.objects.get(phone_number=content["customer"])
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        sales = Sale.objects.create(**content)
        

        return JsonResponse(
            sales,
            encoder=SalesEncoder,
            safe=False,
        )



@require_http_methods(["DELETE"])
def api_delete_sale(request, id):
    try:
        sale = Sale.objects.get(id=id)
        sale.delete()
        return JsonResponse({"message": "sale deleted successfully"})
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "sale does not exist"},
            status=404,
        )



