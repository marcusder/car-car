import React, { useState, useEffect } from 'react';

function TechnicianForm() {
    const [first_name,setfirstName]= useState('');
    const [last_name, setlastName] = useState('');
    const [employee_id, setEmployeeId] = useState('');




    async function handleSubmit(event) {
        event.preventDefault();
        const data = {}
        data.first_name = first_name
        data.last_name = last_name
        data.employee_id = employee_id

        const hatUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newTechnician = await response.json();
            alert('Technician added successfully!');
            setfirstName('');
            setlastName('');
            setEmployeeId('');
        } else {
            alert('An error occurred while adding technician.');
          }
        };

    function handleFirstNameChange(event) {
        const  value  = event.target.value;
        setfirstName(value);
    }

    function handleLastNameChange(event) {
        const value  = event.target.value;
        setlastName(value);
    }

    function handleEmployeeIdChange(event) {
        const value  = event.target.value;
        setEmployeeId(value);
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Technician</h1>
              <form  onSubmit = {handleSubmit} id="Add-Technician-Form">
                <div className="form-floating mb-3">
                  <input onChange ={handleFirstNameChange}  value = {first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                  <label htmlFor="name">First name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange ={handleLastNameChange} value ={last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                  <label htmlFor="starts">Last name</label>
                </div>
                <div className="form-floating mb-3">
                  <input  onChange ={handleEmployeeIdChange} value ={employee_id} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                  <label htmlFor="ends">Employee ID</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
              </div>
          </div>
        </div>
      );

    }

    export default TechnicianForm;
