import { useState } from 'react';

function SalespersonForm( {onAddSalesperson }) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();

    const data = {
         first_name: firstName,
         last_name: lastName, 
         employee_id: employeeId 
        };

    const response = await fetch("http://localhost:8090/api/salespeople/", {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    });

    if (response.ok) {
      alert('Salesperson added successfully!');
      setFirstName('');
      setLastName('');
      setEmployeeId('');
      onAddSalesperson(); 
    } else {
      alert('An error occurred while adding salesperson.');
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new Salesperson</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input value={firstName} onChange={(e) => setFirstName(e.target.value)} placeholder="First Name" required type="text" name="firstName" id="firstName" className="form-control" />
              <label htmlFor="firstName">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={lastName} onChange={(e) => setLastName(e.target.value)} placeholder="Last Name" required type="text" name="lastName" id="lastName" className="form-control" />
              <label htmlFor="lastName">Last Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={employeeId} onChange={(e) => setEmployeeId(e.target.value)} placeholder="Employee ID" required type="text" name="employeeId" id="employeeId" className="form-control" />
              <label htmlFor="employeeId">Employee ID</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}


export default SalespersonForm;