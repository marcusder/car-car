import { useState, useEffect } from 'react';


function ManufacturerForm( { onAddManu }) {
    const [name, setName] = useState('');

  
    const handleSubmit = async (e) => {
      e.preventDefault();
  
      const data = {
           name: name,
          };
  
      const response = await fetch("http://localhost:8100/api/manufacturers/", {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      });
  
      if (response.ok) {
        alert('Manufacturer added successfully!');
        setName('');
        onAddManu(); // have to add this so re render occur on employee list
      } else {
        alert('An error occurred while adding manufacturer.');
      }
    }
  
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input value={name} onChange={(e) => setName(e.target.value)} placeholder="Manufacturer" required type="text" name="firstName" id="firstName" className="form-control" />
                <label htmlFor="firstName">Enter Manufacturer Name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
  
  
  export default ManufacturerForm;