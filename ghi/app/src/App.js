import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonForm from './SalespersonForm';
import ListSalespeople from './ListSalespeople';
import ListCustomers from './ListCustomers';
import CustomerForm from './CustomerForm';
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import SalesHistory from './SalesHistory';
import ListManufacturers from './Inventory/ListManufacturers';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelList from './Inventory/ModelList';
import AutoList from './Inventory/AutomobileList';
import ModelForm from './Inventory/ModelForm';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import TechnicianForm from './TechnicianForm';
import AutomobileForm from './Inventory/AutomobileForm'
import TechnicianList from './TechnicianList'
import AppointmentHistory from './AppointmentHistory'

function App() {
  const [salespeople, setSalespeople] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [sales, setSales] = useState([]);
  const [manufacturers, setManufacturers] = useState([])
  const [autos, setAutos] = useState([]);
  const [models, setModels] = useState([])

  async function getModels(){
    const response = await fetch ('http://localhost:8100/api/models/')
    if (response.ok){
      const {models} = await response.json()
      setModels(models)
    } else{
      console.error("An error occured fetching models list")
    }

  }

  async function getAutos(){
    const response = await fetch ('http://localhost:8100/api/automobiles/')
    if (response.ok){
      const {autos} = await response.json()
      setAutos(autos)
    } else{
      console.error("An error occured fetching autos list")
    }

  }

  async function getManufacturers(){
    const response = await fetch("http://localhost:8100/api/manufacturers/")
    if (response.ok){
      const {manufacturers} = await response.json();
      setManufacturers(manufacturers);
    } else {
      console.error("An error occured fetching the manufacturer data")
    }

  }

  async function getSalespeople() {
    const response = await fetch("http://localhost:8090/api/salespeople/");
    if (response.ok){
      const { salespeople } = await response.json();
      setSalespeople(salespeople);
    } else {
      console.error("An error occurred fetching the salespeople data");
    }
  }

  async function getCustomers() {
    const response = await fetch("http://localhost:8090/api/customers/");
    if (response.ok){
      const { customers } = await response.json();
      setCustomers(customers);
    } else {
      console.error("An error occurred fetching customer data");
    }
  }


  async function getSales() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok){
      const { sales } = await response.json();
      setSales(sales);
    } else {
      console.error("An error occurred fetching sales data");
    }
  }

  useEffect(() => {
    getSalespeople();
    getCustomers();
    getSales();
    getManufacturers();
    getAutos();
    getModels();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="salespeople" element={<ListSalespeople salespeople={salespeople} />} />
          <Route path="salespeople/new" element={<SalespersonForm onAddSalesperson={getSalespeople} />} />
          <Route path="customers" element={<ListCustomers customers={customers} />} />
          <Route path="customers/new" element={<CustomerForm onAddCustomer={getCustomers} />} />
          <Route path="sales" element={<SalesList sales={sales} />} />
          <Route path="sales/new" element={<SaleForm onAddSale={getSales}  />} />
          <Route path="sales/history" element={<SalesHistory onAddSale={getSales}  />} />
          <Route path="manufacturers" element={<ListManufacturers manufacturers={manufacturers}  />} />
          <Route path="manufacturers/new" element={<ManufacturerForm onAddManu={getManufacturers}  />} />
          <Route path="models" element={<ModelList models={models}  />} />
          <Route path="autos" element={<AutoList autos={autos}  />} />
          <Route path="models/new" element={<ModelForm onAddModel={getModels} manufacturers={manufacturers}  />} />
          <Route path="autos/new" element={<AutomobileForm onAddAutos={getAutos} autos={autos}  />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/create" element={<AppointmentForm />} />
          <Route path="/appointments/history" element={<AppointmentHistory />} />
          <Route path="/technicians/create" element={<TechnicianForm />} />
          <Route path="/technicians/" element={<TechnicianList />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
  }
export default App;
