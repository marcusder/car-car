import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople"> Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/new">Add Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customers/new">Add Customer </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales"> List Sale </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/new"> Record a new sale </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/history"> Salesperson History </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers"> Manufacturers </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new"> Manufacturer Form </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models"> Models </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/models/new"> Add Model </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/autos"> Autos </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/autos/new"> Add Autos </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians"> Technicians </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/technicians/create"> Add Technicians </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments"> Appointments </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/create"> Add Appointments </NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/appointments/history"> Appointments History </NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
