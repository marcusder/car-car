import React, { useState, useEffect } from 'react';

function AppointmentList() {
  const [appointments, setAppointments] = useState([]);
  const [automobiles, setAutomobiles] = useState([]);

  async function loadAppointments() {
    const url = 'http://localhost:8080/api/appointments/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointment);
    } else {

    }
  }

  async function cancelAppointment(appointmentId) {
    const apptURL = `http://localhost:8080/api/appointments/${appointmentId}/`;
    const fetchOptions = {
      method: "PUT",
      body: JSON.stringify({ status: "Cancelled" }),
      headers: { 'Content-Type': 'application/json' }
    };
    const response = await fetch(apptURL, fetchOptions);

    if (response.ok) {
      alert('Appointment successfully cancelled!');
      loadAppointments();
    } else {
      alert('An error occurred while cancelling the appointment.');
    }
  }

  async function finishAppointment(appointmentId) {
    const apptURL = `http://localhost:8080/api/appointments/${appointmentId}/`;
    const fetchOptions = {
      method: "PUT",
      body: JSON.stringify({ status: "Finished" }),
      headers: { 'Content-Type': 'application/json' }
    };
    const response = await fetch(apptURL, fetchOptions);

    if (response.ok) {
      alert('Appointment successfully finished!');
      loadAppointments();
    } else {
      alert('An error occurred while finishing the appointment.');
    }
  }

  async function fetchSales() {
    const salesurl = 'http://localhost:8090/api/sales/';
    const response = await fetch(salesurl);
    if (response.ok) {
      const salesdata = await response.json();
      setAutomobiles(salesdata.sales);

    }
  }

  useEffect(() => {
    loadAppointments();
    fetchSales();
  }, []);
return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Is VIP?</th>
          <th>Customer</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
        </tr>
      </thead>
      <tbody>
        {appointments.filter((appointment) => {
          return appointment.status === "Created";
        }).map(appointment => {
            const is_vip = automobiles.filter((auto)=>{
                return auto.automobile.vin === appointment.vin
            }).length > 0

          return (
            <tr key={appointment.id}>
              <td>{appointment.vin}</td>
              <td>{is_vip ? "Yes" : "No"}</td>
              <td>{appointment.customer}</td>
              <td>{new Date(appointment.date_time).toLocaleDateString()}</td>
              <td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
              <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
              <td>{appointment.reason}</td>
              <td>
                <button onClick={() => cancelAppointment(appointment.id)}>Cancel</button>
              </td>
              <td>
                <button onClick={() => finishAppointment(appointment.id)}>Finish</button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AppointmentList;
